/**
 * Ein Studenten-Programm in Java.
 * Dies ist ein Javadoc-Kommentar.
 *
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 14.10.2020
 */

public abstract class Student implements StudentDao {

    //Eigenschaften/Datenfelder
    private String name;
    private int rollNo;

    //Konstruktor
    public Student() {
    }

    //Methoden

    /**
     * @return Liefert den Namen
     */
    public String getName() {
        return name;
    }

    /**
     * @param name Setzt den Namen
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Liefert die Nummer
     */
    public int getRollNo() {
        return rollNo;
    }

    /**
     * @param rollNo Setzt die Nummer
     */
    public String setRollNo(String rollNo) {
        this.rollNo = Integer.parseInt(String.valueOf(rollNo));
        return rollNo;
    }
}
