/**
 * Ein Studenten-Programm in Java.
 * Dies ist ein Javadoc-Kommentar.
 *
 * @author Christoph Lottersberger
 * @version 1.0
 * @since 14.10.2020
 */

//Importieren

import java.util.List;

//Interface
public interface StudentDao {

    //Methodenköpfe

    /**
     * @return Liefert alle Studenten
     */
    public List getAllStudents();

    public void updateStudentl();

    public void deleteStudent();

    public void addStudent();
}
